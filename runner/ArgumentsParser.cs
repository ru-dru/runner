﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
namespace runner
{
    public class ArgumentsParser
    {
        private string programFileName = "";
        private string programArguments = "";
        private int secondsForWait = 0;

        public Parameters Parse(string[] arguments)
        {
            foreach (var argument in arguments)
            {
                if (IsProperty(argument))
                {
                    ParseProperty(argument);
                }
                else
                {
                    throw new Exception($"'{argument}' не является верным форматом аргументов командной строки");
                }
            }

            return new Parameters(programFileName, programArguments, secondsForWait);
        }

        private bool IsProperty(string argument)
        {
            return
                !string.IsNullOrEmpty(argument) &&
                argument.Count(x => x == '=') == 1 &&
                argument.IndexOf('=') != argument.Length - 1;
        }

        private void ParseProperty(string property)
        {
            string propertyName = GetPropertyName(property);
            string propertyValue = GetPropertyValue(property);

            switch (propertyName)
            {
                case "exe":
                    programFileName = propertyValue;
                    break;
                case "args":
                    programArguments = propertyValue;
                    break;
                case "t":
                    if (!int.TryParse(propertyValue, out secondsForWait))
                        throw new Exception($"'{propertyValue}' не является временем, выраженным целым числом (в секундах)");
                    break;
            }
        }

        private string GetPropertyName(string property)
        {
            return property.Split('=')[0];
        }

        private string GetPropertyValue(string property)
        {
            return property.Split('=')[1];
        }
    }
}
