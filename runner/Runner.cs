﻿using System;
using System.Diagnostics;
using System.IO;

namespace runner
{
    class Runner
    {
        private static ProcessStartInfo processStartInfo(Parameters parametrs)
        {
            var psi = new ProcessStartInfo();
            psi.Arguments = parametrs.ProgramArguments;
            psi.FileName = parametrs.ProgramFileName;

            return psi;
        }

        static void Main(string[] args)
        {
            try
            {
                var parametrs = new ArgumentsParser().Parse(args);
                var proc = new Process();

                proc.StartInfo = processStartInfo(parametrs);

                proc.Start();
                proc.WaitForExit(parametrs.SecondsForWait * 1000);
                proc.Kill();
            }
            catch (Exception e)
            {
                try
                {
                    using (var logFile = new StreamWriter(new FileStream("log.txt", FileMode.Append)))
                    {
                        logFile.WriteLine($"{DateTime.Now.ToString()} - {e.Message}");
                    }
                }
                catch(Exception)
                {
                    ;
                }
            }
        }
    }
}
