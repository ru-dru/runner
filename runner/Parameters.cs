﻿using System;
using System.Collections.Generic;
using System.Text;

namespace runner
{
    public class Parameters
    {
        public string ProgramFileName{ get; }
        public string ProgramArguments { get; }
        public int SecondsForWait { get; }

        public Parameters(
            string programFileName,
            string programArguments,
            int secondsForWait
        )
        {
            ProgramFileName = programFileName;
            ProgramArguments = programArguments;
            SecondsForWait = secondsForWait;
        }

        public Parameters()
        {
            ProgramFileName = "";
            ProgramArguments = "";
            SecondsForWait = 0;
        }
    }
}
